//crie um array com 3 nomes de frutas.Em seguida, use um loop whilw para 
//interar sobre o array e exibir cada fruta em uma linha separada.
namespace exercicio2 {
    let frutas: string[] = ["banana", "morango", "manga"];
    let i: number = 0;

    while (i < frutas.length) {
        console.log(frutas[i]);
        i++;
    }
}