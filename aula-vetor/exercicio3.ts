//Crie um array com 4 objetos, cada um representando um livro 
//com as propriedades titulo e autor. Em seguida, use o método map() 
//para criar um novo array contendo apenas os títulos dos livros.
/*namespace exercicio3 {
    let obj: any = 
    {
        nome: "Helena",
        idade: "16",
        email: "helenaliboni013@gmail.com"
    }
    console.log(obj);
    let livros: any[] = [
        {titulo: "titulo 1", autor: "autor 1"},
        {titulo: "titulo 2", autor: "autor 2"},
        {titulo: "titulo 3", autor: "autor 3"},
        {titulo: "titulo 4", autor: "autor 4"},
        {titulo: "titulo 5", autor: "autor 5"},
    ];
    let autores = livros.map((livro) => { return livro.autor});
    let titulos = livros.map((livro) => { return livro.titulo});
    console.log(autores);
    console.log(titulos);

    /*Crie um array com 4 objetos, cada um representando um livro com as propriedades titulo e autor. Em seguida, use o método map() 
para criar um novo array contendo apenas os títulos dos livros.*/
namespace livros{
    let livros: any[] = [
        {titulo: "Senhor dos aneis", autor: "Autor 3"},
        {titulo: "Harry Potter", autor: "Autor 3"},
        {titulo: "Mulan", autor: "Disney"},
        {titulo: "O pequeno principe", autor: "Antoine de Saint-Exupéry"}
    ]
    /*
    let titulos = livros.map((livro) => {
        return livro.titulo
    })
    let autores = livros.map((livro) => {
        return livro.autor
    })

    console.log(titulos);
    console.log(autores);
    /*Dado um array de objetos livros, contendo os campos titulo e autor, crie um programa em TypeScript que utilize a função filter() 
    para encontrar todos os livros do autor com valor "Autor 3".Em seguida, utilize a função map() para mostrar apenas os títulos dos
    livros encontrados. O resultado deve ser exibido no console.*/

    let autor3 = livros.filter((livro) =>{
        return livro.autor === "Autor 3"
    });

    let titulos3 = autor3.map((pegar) =>
    {
        return pegar.titulo 
    });

    console.log(titulos3);    

}